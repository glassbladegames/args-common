#include <stdlib.h>
#include <string.h>
char* GetArg (int argc, char** argv, char* prefix)
{
	for (int i = 0; i < argc; i++)
	{
		char* prefixptr = prefix;
		char* str = *(argv + i);
		size_t len = strlen(str);
		for (size_t pos = 0; pos < len; pos++, str++)
		{
			char cc = *str;
			char prefcc = *prefixptr++;
			if (prefcc)
			{
				if (cc != prefcc) goto WRONG;
			}
			else return str;
		}
		return str;
		WRONG:;
	}
	return NULL;
}
